import {PaymentAppApplication} from './application';
import {ApplicationConfig} from '@loopback/core';

export {PaymentAppApplication};

export async function main(options: ApplicationConfig = {}) {
  const app = new PaymentAppApplication(options);
  await app.boot();
  await app.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);

  return app;
}
