import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  User,
  Account,
} from '../models';
import {UserRepository} from '../repositories';

export class UserAccountController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @get('/users/{id}/account', {
    responses: {
      '200': {
        description: 'Account belonging to User',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Account)},
          },
        },
      },
    },
  })
  async getAccount(
    @param.path.string('id') id: typeof User.prototype._id,
  ): Promise<Account> {
    return this.userRepository.account(id);
  }
}
