import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Reward,
  User,
} from '../models';
import {RewardRepository} from '../repositories';

export class RewardUserController {
  constructor(
    @repository(RewardRepository)
    public rewardRepository: RewardRepository,
  ) { }

  @get('/rewards/{id}/user', {
    responses: {
      '200': {
        description: 'User belonging to Reward',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(User)},
          },
        },
      },
    },
  })
  async getUser(
    @param.path.string('id') id: typeof Reward.prototype._id,
  ): Promise<User> {
    return this.rewardRepository.user(id);
  }
}
