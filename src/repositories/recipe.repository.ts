import {DefaultCrudRepository} from '@loopback/repository';
import {Recipe, RecipeRelations} from '../models';
import {MongoDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RecipeRepository extends DefaultCrudRepository<
  Recipe,
  typeof Recipe.prototype._id,
  RecipeRelations
> {
  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource,
  ) {
    super(Recipe, dataSource);
  }
}
