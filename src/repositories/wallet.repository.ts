import {DefaultCrudRepository} from '@loopback/repository';
import {Wallet, WalletRelations} from '../models';
import {MongoDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class WalletRepository extends DefaultCrudRepository<
  Wallet,
  typeof Wallet.prototype._id,
  WalletRelations
> {
  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource,
  ) {
    super(Wallet, dataSource);
  }
}
