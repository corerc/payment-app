import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {Reward, RewardRelations, User} from '../models';
import {MongoDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {UserRepository} from './user.repository';

export class RewardRepository extends DefaultCrudRepository<
  Reward,
  typeof Reward.prototype._id,
  RewardRelations
> {

  public readonly user: BelongsToAccessor<User, typeof Reward.prototype._id>;

  constructor(
    @inject('datasources.mongo') dataSource: MongoDataSource, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Reward, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter,);
  }
}
